$(document).ready(function() {
 

     //CarregarConsultas("05/04/2018 23:17:56","06/04/2018 23:55:56",2);
     CarregarConsultas($("#txtdtInicio").val(), $("#txtdtFim").val(), $("#cbMedico").val());
    CarregarComboMedico();


    $("#btnPesquisar").click(function(){
        $("#txtdtInicio").val( $("#txtdtInicio").val().length == 10 ? $("#txtdtInicio").val() + " 00:00:00":$("#txtdtInicio").val());
        $("#txtdtFim").val( $("#txtdtFim").val().length == 10 ? $("#txtdtFim").val() + " 23:59:59":$("#txtdtFim").val());
        CarregarConsultas($("#txtdtInicio").val(), $("#txtdtFim").val(), $("#cbMedico").val());
        
    });

    function CarregarConsultas(dataInicio,dataFim,idMedico){

        $.ajax({
            url: "http://localhost:8080/consultasmedicas/listar",
            data:{dataInicio: dataInicio, dataFim: dataFim, idMedico:idMedico},
            type: 'POST',
            dataType: 'json',
            success: function(res) {            
                
                var html = "";
                $("#tb").html("");
               if(res.lista.length > 0){
                
               
                for (var i = 0; i <  res.lista.length; i++) { 
                     console.log(res.lista[0].id);
                     html = html +"<tr>";
                     html = html +"<td>"+ res.lista[i].id +"</td>";
                     html = html +"<td>"+ res.lista[i].nomePaciente +"</td>";
                     html = html +"<td>"+ res.lista[i].data +"</td>";
                     html = html +"<td>"+ res.lista[i].especialidade +"</td>";
                     html = html +"<td>"+ res.lista[i].nomeMedico +"</td>";
                     html = html +"<td>"+ res.lista[i].crm +"</td>";
                     html = html +"</tr>";
                }
    
                $("#tb").html(html);
                $("#tbConsulta").show();
                $("#dvMensagem").hide();
                }else{
                    $("#tbConsulta").hide();
                    $("#dvMensagem").show();
                    
                }

            }
        });
    }

    function CarregarComboMedico(){

        $.ajax({
            url: "http://localhost:8080/medicos/listar",            
            dataType: 'json',
            success: function(res) {
               
                var option = '<option value ="0" >Todos</option>';
                $.each(res.lista, function(i, obj){
                    option += '<option value="'+obj.id+'">'+obj.nome+'</option>';
                })
                $('#cbMedico').html(option).show();
            }
        });
    }

} );