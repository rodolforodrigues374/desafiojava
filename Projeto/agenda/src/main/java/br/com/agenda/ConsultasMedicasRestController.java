package br.com.agenda;

import java.util.ArrayList;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.database.DataClient;
import br.com.model.Consulta;
import br.com.model.ConsultaViewModel;
import br.com.model.Mensagem;
import br.com.utils.Util;

@RestController
public class ConsultasMedicasRestController {

	public DataClient data;
	public Util util;

	public ConsultasMedicasRestController() {
		data = new DataClient();
		util = new Util();
	}

	@RequestMapping(value = "/consultasmedicas/listar", method = RequestMethod.POST)
	public ResponseEntity<ConsultaViewModel> listar(String dataInicio, String dataFim, Integer idMedico) {

		ConsultaViewModel model = new ConsultaViewModel();
		try {

			model.lista = data.ListarConsultas(util.FormataData(dataInicio), util.FormataData(dataFim), idMedico);
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.OK);
	}

	@RequestMapping(value = "/consultasmedicas/cadastrar", method = RequestMethod.POST)
	public ResponseEntity<ConsultaViewModel> Cadastrar(Consulta consulta) {

		ConsultaViewModel model = new ConsultaViewModel();	

		if (VerificaIntervalo(consulta)) {

			model.mensagem = new Mensagem(false,
					"A consulta deve respeitar um tempo mínimo de 15 minutos para o mesmo consultório.");
			return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.OK);

		} else if (VerificaConsultaPorPaciente(consulta)) {

			model.mensagem = new Mensagem(false, "O paciente não pode marcar duas consultas no mesmo dia.");
			return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.OK);

		} else if (VerificaQuantidadeConsultaPorConsultorio(consulta)) {

			model.mensagem = new Mensagem(false,
					"O consultório está com a capacidade máxima, 12 consultas agendas no mesmo dia.");
			return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.OK);

		} else {
			if (data.CadastrarConsulta(consulta)) {
				model.mensagem = new Mensagem(true, "Cadastro realizado com sucesso!");
			} else {
				model.mensagem = new Mensagem(false, "Ocorreu um erro ao tentar cadastrar a consulta!");
			}
		}
		return new ResponseEntity<ConsultaViewModel>(model, HttpStatus.OK);

	}
 	
	// Verifica o intervalo das consultas no mesmo consultório, deve respeitar um
	// tempo mínimo de 15 minutos
	public Boolean VerificaIntervalo(Consulta consulta) {

		try {

			ArrayList<Consulta> lista = new ArrayList<Consulta>();
			lista = data.VerificaIntervalTempo(consulta.getData(), consulta.getIdMedico(), consulta.getIdConsultorio(),
					consulta.getIdPaciente());

			if (ConsultaEspecialidadeMedicoContemCirurgicao(consulta)) {

				for (Consulta model : lista) {

					if (model.getEspecialidade().contains("Cirurgião")) {
						return false;
					} else {
						return true;
					}
				}
			} else if (lista.size() > 0) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return false;
	}

	// O mesmo paciente não pode marcar duas consultas no mesmo dia.
	public Boolean VerificaConsultaPorPaciente(Consulta consulta) {

		try {

			ArrayList<Consulta> lista = new ArrayList<Consulta>();
			lista = data.VerificaConsultaPorPaciente(consulta.getData(), consulta.getIdPaciente());

			if (lista.size() > 0) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return false;
	}

	// As consultas devem se limitar a no máximo 12 por dia num mesmo consultório
	public Boolean VerificaQuantidadeConsultaPorConsultorio(Consulta consulta) {

		try {

			ArrayList<Consulta> lista = new ArrayList<Consulta>();
			lista = data.VerificaQuantidadeConsultaPorConsultorio(consulta.getData(), consulta.getIdConsultorio());

			if (lista.size() > 12) {
				return true;
			}

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return false;
	}

	//Verifica se a especialidade é Cirurgião
	public Boolean ConsultaEspecialidadeMedicoContemCirurgicao(Consulta consulta) {

		return data.ConsultaEspecialidadeMedicoContemCirurgicao(consulta.getIdMedico());
	}

}