package br.com.agenda;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.database.DataClient;
import br.com.model.Mensagem;
import br.com.model.PacienteViewModel;

@RestController
public class PacienteRestController {
	
	public DataClient data;

	public PacienteRestController() {
		data = new DataClient();
	}
	
	@RequestMapping(value = "/paciente/listar", method = RequestMethod.GET)
	public ResponseEntity<PacienteViewModel> listar() {

		PacienteViewModel model = new PacienteViewModel();
		try {

			model.lista = data.ListarPaciente();
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<PacienteViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<PacienteViewModel>(model, HttpStatus.OK);
	}
}
