package br.com.agenda;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.database.DataClient;
import br.com.model.Medico;
import br.com.model.MedicoViewModel;
import br.com.model.Mensagem;

@RestController
public class MedicoRestController {

	public DataClient data;

	public MedicoRestController() {
		data = new DataClient();
	}

	@RequestMapping(value = "/medicos/listar", method = RequestMethod.GET)
	public ResponseEntity<MedicoViewModel> listar() {

		MedicoViewModel model = new MedicoViewModel();
		try {

			model.lista = data.ListarMedicos();
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<MedicoViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<MedicoViewModel>(model, HttpStatus.OK);
	}

	@RequestMapping(value = "/medicos/cadastrar", method = RequestMethod.POST)
	public ResponseEntity<MedicoViewModel> Cadastrar(Medico medico) {

		MedicoViewModel model = new MedicoViewModel();

		try {
			data.CadastrarMedico(medico);
			model.lista = data.ListarMedicos();
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<MedicoViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<MedicoViewModel>(model, HttpStatus.OK);
	}
}