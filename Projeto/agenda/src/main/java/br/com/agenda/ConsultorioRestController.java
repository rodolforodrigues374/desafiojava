package br.com.agenda;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import br.com.database.DataClient;
import br.com.model.Consultorio;
import br.com.model.ConsultorioViewModel;
import br.com.model.Mensagem;

@RestController
public class ConsultorioRestController {

	public DataClient data;

	public ConsultorioRestController() {
		data = new DataClient();
	}

	@RequestMapping(value = "/consultorio/listar", method = RequestMethod.GET)
	public ResponseEntity<ConsultorioViewModel> listar() {

		ConsultorioViewModel model = new ConsultorioViewModel();
		try {

			model.lista = data.ListarConsultorio();
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<ConsultorioViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ConsultorioViewModel>(model, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/consultorio/cadastrar", method = RequestMethod.POST)
	public ResponseEntity<ConsultorioViewModel> Cadastrar(Consultorio consultorio) {

		ConsultorioViewModel model = new ConsultorioViewModel();

		try {
			data.CadastrarConsultorio(consultorio);
			model.lista = data.ListarConsultorio();
			model.mensagem = new Mensagem(true, "OK");

		} catch (Exception e) {
			e.printStackTrace(System.out);
			model.mensagem = new Mensagem(false, e.getMessage());
			return new ResponseEntity<ConsultorioViewModel>(model, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<ConsultorioViewModel>(model, HttpStatus.OK);
	}

}
