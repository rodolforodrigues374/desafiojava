package br.com.model;

import java.util.Date;

public class Paciente {

	public Paciente() {

	}

	public Paciente(Integer id, String nomePaciente) {
		this.setId(id);
		this.setNomePaciente(nomePaciente);
		 
	}

	private Integer id;
	private String nomePaciente;
	 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomePaciente() {
		return nomePaciente;
	}

	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	} 
}
