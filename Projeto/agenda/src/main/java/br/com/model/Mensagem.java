package br.com.model;

public class Mensagem {

	public Mensagem() {

	}

	public Mensagem(Boolean status, String descricao) {
		this.setStatus(status);;
		this.setDescricao(descricao);
	}
	
	private Boolean status;
	private String descricao;
	
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
