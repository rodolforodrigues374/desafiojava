package br.com.model;

public class Consulta {

	public Consulta() {

	}

	public Consulta(Integer id, String nomePaciente, String especialidade, String nomeMedico, String crm, String data,
			String consultorio) {
		this.setId(id);
		this.setNomePaciente(nomePaciente);
		this.setEspecialidade(especialidade);
		this.setNomeMedico(nomeMedico);
		this.setCrm(crm);
		this.setData(data);
		this.setConsultorio(consultorio);
	}

	private Integer id;
	private String nomePaciente;
	private String especialidade;
	private String nomeMedico;
	private String crm;
	private String data;
	private String consultorio;
	private Integer idMedico;
	private Integer idConsultorio;
	private Integer idPaciente;

	public String getNomeMedico() {
		return nomeMedico;
	}

	public void setNomeMedico(String nomeMedico) {
		this.nomeMedico = nomeMedico;
	}

	public String getNomePaciente() {
		return nomePaciente;
	}

	public void setNomePaciente(String nomePaciente) {
		this.nomePaciente = nomePaciente;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(String consultorio) {
		this.consultorio = consultorio;
	}

	public Integer getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(Integer idMedico) {
		this.idMedico = idMedico;
	}

	public Integer getIdConsultorio() {
		return idConsultorio;
	}

	public void setIdConsultorio(Integer idConsultorio) {
		this.idConsultorio = idConsultorio;
	}

	public Integer getIdPaciente() {
		return idPaciente;
	}

	public void setIdPaciente(Integer idPaciente) {
		this.idPaciente = idPaciente;
	}

}
