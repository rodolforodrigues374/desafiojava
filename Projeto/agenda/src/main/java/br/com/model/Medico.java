package br.com.model;

public class Medico {

	public Medico() {

	}

	public Medico(Integer id, String nome, String crm,String especialidade, Integer idade) {
		this.setId(id);
		this.setNome(nome);
		this.setCrm(crm);
		this.setEspecialidade(especialidade);
		this.setIdade(idade);
	}

	private Integer id;
	private String nome;
	private String crm;
	private String especialidade;
	private Integer idade;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCrm() {
		return crm;
	}

	public void setCrm(String crm) {
		this.crm = crm;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

}
