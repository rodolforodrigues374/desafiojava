package br.com.model;

public class Consultorio {

	public Consultorio() {

	}

	public Consultorio(Integer id, String nomeConsultorio ) {
		this.setId(id);
		this.setNomeConsultorio(nomeConsultorio); 
	}
	
	private Integer id;
	private String nomeConsultorio;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomeConsultorio() {
		return nomeConsultorio;
	}
	public void setNomeConsultorio(String nomeConsultorio) {
		this.nomeConsultorio = nomeConsultorio;
	}
}
