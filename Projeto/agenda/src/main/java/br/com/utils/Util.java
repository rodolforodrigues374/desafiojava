package br.com.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.Date;

public class Util {

	public String CalculaIntervaloTempo(String data) {
		LocalTime time = LocalTime.parse(data.substring(10).trim());
		String minuto = time.minus(Duration.ofMinutes(15)).toString();

		String dt = FormataData(data);
		return dt.substring(0, 10) + " " + minuto + (minuto.length() == 5 ? ":00" : "");
	}

	public String FormataData(String data) {

		try {

			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date dt = df.parse(data);

			return df2.format(dt);

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return "";
	}
	
	public String FormataData2(String data) {

		try {

			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd");
			Date dt = df.parse(data);

			return df2.format(dt);

		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return "";
	}

}
