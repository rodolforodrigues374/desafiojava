package br.com.database;

import java.sql.Connection;
import java.sql.DriverManager;

public class BaseDataClient {

	public Connection Conectar() {

		Connection con = null;
		try {

			Class.forName("org.hsqldb.jdbc.JDBCDriver");
			con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/testdb", "SA", "");

			return con;
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}
		return con;
	}

}
