package br.com.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import br.com.model.Consulta;
import br.com.model.Consultorio;
import br.com.model.Medico;
import br.com.model.Paciente;
import br.com.utils.Util;

public class DataClient {

	ResultSet result = null;
	Statement stmt = null;
	Connection con = null;
	BaseDataClient db = null;
	Util util;

	public DataClient() {
		db = new BaseDataClient();
		util = new Util();
	}

	public ArrayList<Consulta> ListarConsultas(String dataInicio, String dataFim, Integer idMedico) {

		ArrayList<Consulta> lista = new ArrayList<Consulta>();
		try {

			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				String query = "SELECT ";
				query = query + "c.id, ";
				query = query + "p.nomePaciente, ";
				query = query + "m.especialidade, ";
				query = query + "m.nome, ";
				query = query + "m.crm, ";
				query = query + "c.data, ";
				query = query + "co.nomeConsultorio ";
				query = query + "FROM consulta_tb c ";
				query = query + "INNER JOIN medico_tb m ON m.id = c.idMedico ";
				query = query + "INNER JOIN paciente_tb p on p.id = c.idPaciente ";
				query = query + "INNER JOIN consultorio_tb co on co.id = c.idConsultorio ";
				query = query + "WHERE c.idMedico = " + (idMedico == null || idMedico == 0 ? " c.idMedico " : idMedico);

				if (dataInicio.length() > 0 && dataFim.length() == 0) {
					query = query + " AND c.data >= '" + dataInicio + "' ";
				} else if (dataFim.length() > 0 && dataInicio.length() == 0) {
					query = query + " AND c.data >= '" + dataFim + "' ";
				} else if (dataFim.length() > 0 && dataInicio.length() > 0) {
					query = query + " AND c.data >= '" + dataInicio + "' and c.data <= '" + dataFim + "' ";
				}

				query = query + "ORDER BY c.data ";

				result = stmt.executeQuery(query);

				while (result.next()) {
					lista.add(new Consulta(result.getInt("id"), result.getString("nomePaciente"),
							result.getString("especialidade"), result.getString("nome"), result.getString("crm"),
							result.getString("data"), result.getString("nomeConsultorio")));

				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Consulta> VerificaIntervalTempo(String data, Integer idMedico, Integer idConsultorio,
			Integer idPaciente) {

		ArrayList<Consulta> lista = new ArrayList<Consulta>();
		try {

			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				String query = "SELECT ";
				query = query + "c.id, ";
				query = query + "p.nomePaciente, ";
				query = query + "m.especialidade, ";
				query = query + "m.nome, ";
				query = query + "m.crm, ";
				query = query + "c.data, ";
				query = query + "co.nomeConsultorio ";
				query = query + " FROM consulta_tb c ";
				query = query + " INNER JOIN medico_tb m ON m.id = c.idMedico ";
				query = query + " INNER JOIN paciente_tb p on p.id = c.idPaciente ";
				query = query + " INNER JOIN consultorio_tb co on co.id = c.idConsultorio ";				
				query = query + " AND c.idConsultorio = " + idConsultorio;
				query = query + " AND c.data >= '" + util.CalculaIntervaloTempo(data) + "' and c.data <= '"
						+ util.FormataData(data) + "' ";
				query = query + " ORDER BY c.data ";

				result = stmt.executeQuery(query);

				while (result.next()) {
					lista.add(new Consulta(result.getInt("id"), result.getString("nomePaciente"),
							result.getString("especialidade"), result.getString("nome"), result.getString("crm"),
							result.getString("data"), result.getString("nomeConsultorio")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Consulta> VerificaConsultaPorPaciente(String data, Integer idPaciente) {

		ArrayList<Consulta> lista = new ArrayList<Consulta>();
		try {

			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				String query = "SELECT ";
				query = query + " c.id, ";
				query = query + " p.nomePaciente, ";
				query = query + " m.especialidade, ";
				query = query + " m.nome, ";
				query = query + " m.crm, ";
				query = query + " c.data, ";
				query = query + " co.nomeConsultorio ";
				query = query + " FROM consulta_tb c ";
				query = query + " INNER JOIN medico_tb m ON m.id = c.idMedico ";
				query = query + " INNER JOIN paciente_tb p on p.id = c.idPaciente ";
				query = query + " INNER JOIN consultorio_tb co on co.id = c.idConsultorio ";
				query = query + " WHERE c.idPaciente = " + idPaciente;
				query = query + " AND trunc(c.data) = '" + util.FormataData2(data) + "'";
				query = query + " ORDER BY c.data ";

				result = stmt.executeQuery(query);

				while (result.next()) {
					lista.add(new Consulta(result.getInt("id"), result.getString("nomePaciente"),
							result.getString("especialidade"), result.getString("nome"), result.getString("crm"),
							result.getString("data"), result.getString("nomeConsultorio")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Consulta> VerificaQuantidadeConsultaPorConsultorio(String data, Integer idConsultorio) {

		ArrayList<Consulta> lista = new ArrayList<Consulta>();
		try {

			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				String query = "SELECT ";
				query = query + " c.id, ";
				query = query + " p.nomePaciente, ";
				query = query + " m.especialidade, ";
				query = query + " m.nome, ";
				query = query + " m.crm, ";
				query = query + " c.data, ";
				query = query + " co.nomeConsultorio ";
				query = query + " FROM consulta_tb c ";
				query = query + " INNER JOIN medico_tb m ON m.id = c.idMedico ";
				query = query + " INNER JOIN paciente_tb p on p.id = c.idPaciente ";
				query = query + " INNER JOIN consultorio_tb co on co.id = c.idConsultorio ";
				query = query + " WHERE c.idConsultorio = " + idConsultorio;
				query = query + " AND trunc(c.data) = '" + util.FormataData2(data) + "'";
				query = query + " ORDER BY c.data ";

				result = stmt.executeQuery(query);

				while (result.next()) {
					lista.add(new Consulta(result.getInt("id"), result.getString("nomePaciente"),
							result.getString("especialidade"), result.getString("nome"), result.getString("crm"),
							result.getString("data"), result.getString("nomeConsultorio")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Paciente> ListarPaciente() {

		ArrayList<Paciente> lista = new ArrayList<Paciente>();
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery("select * from paciente_tb");

				while (result.next()) {
					lista.add(new Paciente(result.getInt("id"), result.getString("nomePaciente")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Consultorio> ListarConsultorio() {

		ArrayList<Consultorio> lista = new ArrayList<Consultorio>();
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery("select * from consultorio_tb");

				while (result.next()) {
					lista.add(new Consultorio(result.getInt("id"), result.getString("nomeConsultorio")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public ArrayList<Medico> ListarMedicos() {

		ArrayList<Medico> lista = new ArrayList<Medico>();
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery("select * from medico_tb");

				while (result.next()) {
					lista.add(new Medico(result.getInt("id"), result.getString("nome"), result.getString("crm"),
							result.getString("especialidade"), result.getInt("idade")));
				}

				return lista;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return lista;
	}

	public Boolean CadastrarMedico(Medico medico) {
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				stmt.executeUpdate("INSERT INTO medico_tb (nome, crm, especialidade, idade) VALUES('"
						+ medico.getNome() + "', '" + medico.getCrm() + "','" + medico.getEspecialidade() + "', "
						+ medico.getIdade() + ");");

				return true;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return false;
	}

	public Boolean CadastrarConsulta(Consulta consulta) {
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				stmt.executeUpdate("INSERT INTO consulta_tb (idPaciente, idMedico, idConsultorio, data) VALUES('"
								+ consulta.getIdPaciente() + "', '" + consulta.getIdMedico() + "','"
								+ consulta.getIdConsultorio() + "', '" + util.FormataData(consulta.getData()) + "');");

				return true;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return false;
	}

	public Boolean CadastrarConsultorio(Consultorio consultorio) {
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				stmt.executeUpdate("INSERT INTO consultorio_tb (nomeConsultorio) VALUES('"
						+ consultorio.getNomeConsultorio() + "');");

				return true;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return false;
	}
	
	public Boolean ConsultaEspecialidadeMedicoContemCirurgicao(Integer idMedico) {

 
		try {
			con = db.Conectar();

			if (con != null) {

				stmt = con.createStatement();
				result = stmt.executeQuery("select * from medico_tb where especialidade = 'Cirurgião' and id = " + idMedico);

				while (result.next()) {
					return true;
				}

				return false;

			}
		} catch (Exception e) {
			e.printStackTrace(System.out);
		}

		return false;
	}
}
