$(document).ready(function() { 

    var d = new Date();
    $("#txtdtInicio").val(d.toLocaleDateString() + " 00:00:00");
    $("#txtdtFim").val(d.toLocaleDateString()+ " 23:59:59");

    CarregarConsultas($("#txtdtInicio").val(), $("#txtdtFim").val(), $("#cbMedico").val());
    CarregarComboMedico();

    $("#btnPesquisar").click(function(){
        
        $("#txtdtInicio").val( $("#txtdtInicio").val().length == 10 ? $("#txtdtInicio").val() + " 00:00:00":$("#txtdtInicio").val());
        $("#txtdtFim").val( $("#txtdtFim").val().length == 10 ? $("#txtdtFim").val() + " 23:59:59":$("#txtdtFim").val());
        CarregarConsultas($("#txtdtInicio").val(), $("#txtdtFim").val(), $("#cbMedico").val());
        
    });

    function CarregarConsultas(dataInicio,dataFim,idMedico){

        $.ajax({
            url: "http://localhost:8080/consultasmedicas/listar",
            data:{dataInicio: dataInicio, dataFim: dataFim, idMedico:idMedico},
            type: 'POST',
            dataType: 'json',
            success: function(res) {            
                
                var html = "";
                $("#tb").html("");
               if(res.lista.length > 0){
             
                for (var i = 0; i <  res.lista.length; i++) { 
                    
                     html = html +"<tr>";
                     html = html +"<td>"+ res.lista[i].id +"</td>";
                     html = html +"<td>"+ res.lista[i].nomePaciente +"</td>";
                     html = html +"<td>"+ formataData(res.lista[i].data) +"</td>";
                     html = html +"<td>"+ res.lista[i].especialidade +"</td>";
                     html = html +"<td>"+ res.lista[i].nomeMedico +"</td>";
                     html = html +"<td>"+ res.lista[i].consultorio +"</td>";
                     html = html +"<td>"+ res.lista[i].crm +"</td>";
                     html = html +"<td>"+ VerificaSeConsultaFoiAtendida(res.lista[i].data) +"</td>";
                     html = html +"</tr>";
                }
    
                $("#tb").html(html);
                $("#tbConsulta").show();
                $("#dvMensagem").hide();
                }else{
                    $("#tbConsulta").hide();
                    $("#dvMensagem").show();
                    
                }

            }
        });
    }

    function CarregarComboMedico(){

        $.ajax({
            url: "http://localhost:8080/medicos/listar",            
            dataType: 'json',
            success: function(res) {
               
                var option = '<option value ="0" >Todos</option>';
                $.each(res.lista, function(i, obj){
                    option += '<option value="'+obj.id+'">'+obj.nome+'</option>';
                })
                $('#cbMedico').html(option).show();
            }
        });
    }

    function formataData(data){        
        return new Date(data).toLocaleString();
    }

    function VerificaSeConsultaFoiAtendida(data)
    {
        var d = new Date(new Date().toLocaleString());
        var d1 = new Date(formataData(data)); 

        if (d.toLocaleString() >= d1.toLocaleString()){
            return "<span class='badge badge-success'>Atendida</span>";
        }else{
            return "<span class='badge badge-warning'>Aguardando Atendimento</span>";
        }
    }

} );