$(document).ready(function() {  

    LimaparTela();

    $("#btnSalvar").click(function(){
        Salvar()
    });

    function Salvar(){
        if( CamposObrigatorios()){
            $.ajax({
                url: "http://localhost:8080/medicos/cadastrar",
                data:{id: $("#txtCodigo").val(), nome: $("#txtNome").val(),crm: $("#txtCrm").val(),especialidade: $("#txtEspecialidade").val(),idade: $("#txtIdade").val()},
                type: 'POST',
                dataType: 'json',
                success: function(res) {   
                    console.log(res);         
                    alert(res.mensagem.descricao)
                    LimaparTela();
                }
            });
        }else{
            alert("Preencha todos os campos!");
        }    
    }

    

    function LimaparTela(){
        
        $("#txtNome").val("");
        $("#txtCrm").val("");
        $("#txtEspecialidade").val("");
        $("#txtIdade").val(""); 
    }

    function CamposObrigatorios(){
       if( $("#txtNome").val() == "" ||
        $("#txtCrm").val() == "" ||
        $("#txtEspecialidade").val() == "" ||
        $("#txtIdade").val() == "" )
        {
            return false;
        } 
        
        return true;
    }

} );