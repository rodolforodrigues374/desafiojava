$(document).ready(function() {
     
    LimaparTela();

    $("#btnSalvar").click(function(){
        Salvar()
    });

    function Salvar(){
       if( CamposObrigatorios()){
            $.ajax({
                url: "http://localhost:8080/consultasmedicas/cadastrar",
                data:{data: $("#txtData").val(),idMedico: $("#cbMedico").val(),idConsultorio: $("#cbConsultorio").val(),idPaciente: $("#cbPaciente").val()},
                type: 'POST',
                dataType: 'json',
                success: function(res) {   
                    console.log(res);         
                    alert(res.mensagem.descricao)
                    if(res.mensagem.descricao == "OK"){
                    LimaparTela();
                    }
                }
            });
        }else{
            alert("Preencha todos os campos!");
        }
    }

    function CarregarComboMedico(){

        $.ajax({
            url: "http://localhost:8080/medicos/listar",            
            dataType: 'json',
            success: function(res) {
               
                var option = '<option value ="0" >Selecione o Médico</option>';
                $.each(res.lista, function(i, obj){
                    option += '<option value="'+obj.id+'">'+obj.nome+'</option>';
                })
                $('#cbMedico').html(option).show();
            }
        });
    }

    function CarregarComboPaciente(){

        $.ajax({
            url: "http://localhost:8080/paciente/listar",            
            dataType: 'json',
            success: function(res) {
               
                var option = '<option value ="0" >Selecione o Paciente</option>';
                $.each(res.lista, function(i, obj){
                    option += '<option value="'+obj.id+'">'+obj.nomePaciente+'</option>';
                })
                $('#cbPaciente').html(option).show();
            }
        });
    }

    function CarregarComboConsultorio(){

        $.ajax({
            url: "http://localhost:8080/consultorio/listar",            
            dataType: 'json',
            success: function(res) {
               
                var option = '<option value ="0" >Selecione o Consultório</option>';
                $.each(res.lista, function(i, obj){
                    option += '<option value="'+obj.id+'">'+obj.nomeConsultorio+'</option>';
                })
                $('#cbConsultorio').html(option).show();
            }
        });
    }

    function LimaparTela(){
        $("#txtCodigo").val("");
        $("#txtData").val("");
        $("#cbMedico").val("");
        $("#cbConsultorio").val("");
        $("#cbPaciente").val("");
        CarregarComboMedico();
        CarregarComboConsultorio();
        CarregarComboPaciente();
    }

    function CamposObrigatorios(){
        
        if( $("#txtData").val() == ""
        || $("#cbMedico").val() == "0"
        || $("#cbConsultorio").val() == "0"
        || $("#cbPaciente").val() == "0") 
        {
            return false;
        }

        return true;
    }

} );