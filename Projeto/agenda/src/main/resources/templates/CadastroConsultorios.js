$(document).ready(function() { 

    $("#btnSalvar").click(function(){
        Salvar()
    });

    function Salvar(){
        if( CamposObrigatorios()){
            $.ajax({
                url: "http://localhost:8080/consultorio/cadastrar",
                data:{id: $("#txtCodigo").val(), nomeConsultorio: $("#txtNome").val()},
                type: 'POST',
                dataType: 'json',
                success: function(res) {   
                    console.log(res);         
                    alert(res.mensagem.descricao)
                    LimaparTela();
                }
            });
        }else{
            alert("Preencha todos os campos!");
        } 
    }    

    function LimaparTela(){       
        $("#txtNome").val(""); 
    }

    function CamposObrigatorios(){
        if( $("#txtNome").val() == "")
         {
             return false;
         } 
         
         return true;
     }

} );