**Desafio Java - Logus Retail ** 

---

## Montar Ambiente e Evidências do Teste 

Montando o ambiente para validar o teste .

1. Descompactar o arquivo **hsqldb-2.4.0.rar** no ** diretório C:\ **.
2. Acesse o diretório **C:\hsqldb-2.4.0\hsqldb\hsqldb** com prompt e execute o seguinte comando abaixo.
	
	*java -classpath C:\hsqldb-2.4.0\hsqldb\lib\hsqldb.jar org.hsqldb.server.Server --database.0
	file:hsqldb/agendadb --dbname.0 testdb*

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/01.jpg)
	
	Devera aparecer a tela abaixo mostrando que banco foi iniciado.
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/02.jpg)
	
	
3. Click no arquivo **C:\hsqldb-2.4.0\hsqldb\bin\runManagerSwing.bat** para abrir o HSQL Database Manager.

	Para conectar no banco preencha os campos conforme o print abaixo.
	
	* **Setting Name:** agenda_settings
	* **Type:** HSQL Database Engine Standalone
	* **Driver:** org.hsqldb.jdbcDriver
	* **URL:** jdbc:hsqldb:hsql://localhost/testdb
	* **User:** SA
	* **Passoword:**

	Segue a tela de exemplo:
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/03.jpg)

4. Click no botão OK, devera conectar no banco e carregar 4 tabelas.

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/04.jpg)

5. Clonar o projeto **desafiojava/Projeto/agenda** em **c:\eclipse-workspace** de modo que fique da seguinte estrutura **c:\eclipse-workspace\agenda**.

	Abra o projeto no eclipse e click em Run As, conforme o print.
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/05.jpg)
	
	
6. Verifique se o servidor foi iniciado com sucesso.

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/06.jpg)

7. Acesse o diretório abaixo abra o arquivo **ConsultasAgendadas.html** no browser.

	**c:\eclipse-workspace\agenda\src\main\resources\templates\ConsultasAgendadas.html**

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/07.jpg)
	
8. Veja no console do navegar se apareceu o erro **Access-Control-Allow-Origin**, se sim libere o acesso conforme o próximo passo.
  
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/08.jpg)

9. Para liberar o acesso use o extensor **Allow-Control-Allow-Origin: * ** do Chrome Web Store.

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/09.jpg)
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/10.jpg)
	
10. Atualize o seu navegar para acessar o sistema, segue as telas evidenciadas.

	**Tela de Consultas Agendadas**
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/11.jpg)
	
	**Tela de Cadastro de Consultas**
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/12.jpg)
		
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/13.jpg)
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/14.jpg)
	
	**Tela de Cadastro de Médicos**
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/15.jpg)
	
	**Tela de Consulta de Médicos**
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/16.jpg)
	
	**Tela de Cadastro de Consultorios**
	
	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/17.jpg)
	
	**Tela de Consulta de Consultorios**
	
	![alt tag](https://bitbucket.org/rodolforodrigues374/desafiojava/raw/fd3f10b1ece4507ff58c5af66018a5d9857b4aed/imagem/18.1.jpg)

11. Teste no Postman

	![alt tag](https://bitbucket.org//rodolforodrigues374/desafiojava/raw/82148ac5001603db132b987d9a6f08ed1bb5c160/imagem/19.jpg)
 
